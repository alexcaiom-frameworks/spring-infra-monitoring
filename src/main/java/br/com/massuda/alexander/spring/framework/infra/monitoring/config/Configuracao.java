/**
 * 
 */
package br.com.massuda.alexander.spring.framework.infra.monitoring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * @author Alex
 *
 */
@Configuration("ConfiguracaoMonitoracao")
@EnableAdminServer
//@ComponentScan(basePackages = {"de.codecentric.boot.admin.server"})
public class Configuracao {

	
}
